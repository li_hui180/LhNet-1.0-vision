import gc
import inspect
import json
import os
from collections import Counter

import imageio
import torch
from imageio import imread, imwrite
from torch.nn.functional import binary_cross_entropy_with_logits, mse_loss
from torch.optim import Adam
from tqdm import tqdm

DEFAULT_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    'train')

METRIC_FIELDS = [
    'val.encoder_mse',
    'val.decoder_loss',
    'val.decoder_acc',
    'val.cover_score',
    'val.generated_score',
    'val.ssim',
    'val.psnr',
    'val.bpp',
    'train.encoder_mse',
    'train.decoder_loss',
    'train.decoder_acc',
    'train.cover_score',
    'train.generated_score',
]
WEIGHT_FIELDS = [
    'high',
    'width',
    'channel',
    'tensor',
]
def main()
    self.fit_metrics['epoch'] = epoch
    metrics_path = os.path.join(self.log_dir, 'metrics.log')
    with open(metrics_path, 'w') as metrics_file:
        json.dump(self.history, metrics_file, indent=4)
if __name__ == '__main__':
    main()
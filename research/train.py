#!/usr/bin/env python3
import argparse
import json
import os
from time import time

import torch

from steganogan import SteganoGAN
from steganogan.critics import BasicCritic
from steganogan.decoders import DenseDecoder
from steganogan.encoders import BasicEncoder, DenseEncoder, ResidualEncoder
from steganogan.loader import DataLoader


def main():
    torch.manual_seed(42)


    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs','-t', default=200, type=int)
    parser.add_argument('--encoder', '-e',default="res", type=str)
    parser.add_argument('--data_depth','-d', default=1, type=int)
    parser.add_argument('--hidden_size','-s', default=32, type=int)
    parser.add_argument('--batch_size', default=8, type=int)
    parser.add_argument('--dataset', '-a',default="div2k", type=str)
    parser.add_argument('--output', default=False, type=str)
    parser.add_argument('--gpu_id','-i', default='3', type=str)
    parser.add_argument('--version','-v', default='test', type=str)
    args = parser.parse_args()
    timestamp = "v"+args.version+args.encoder+"-"+"s"+str(args.hidden_size)+"d"+str(args.data_depth)+args.dataset+str(args.epochs)
    #timestamp = timestamp+"-"+str(int(time()));
    train = DataLoader(os.path.join("/mnt/diske/lh/steDta", args.dataset, "train"), shuffle=True,batch_size=args.batch_size)
    validation = DataLoader(os.path.join("/mnt/diske/lh/steDta", args.dataset, "val"), shuffle=False,batch_size=args.batch_size)

    encoder = {
        "basic": BasicEncoder,
        "res": ResidualEncoder,
        "dense": DenseEncoder,
    }[args.encoder]
    steganogan = SteganoGAN(
        data_depth=args.data_depth,
        encoder=encoder,
        decoder=DenseDecoder,
        critic=BasicCritic,
        hidden_size=args.hidden_size,
        cuda=True,
        verbose=True,
        log_dir=os.path.join('/mnt/diske/lh/steDta/models', timestamp),
        cuda_id=args.gpu_id
    )
    with open(os.path.join("/mnt/diske/lh/steDta/models", timestamp, "config.json"), "wt") as fout:
        fout.write(json.dumps(args.__dict__, indent=2, default=lambda o: str(o)))

    steganogan.fit(train, validation, epochs=args.epochs)
    steganogan.save(os.path.join("/mnt/diske/lh/steDta/models", timestamp, "weights.steg"))
    if args.output:
        steganogan.save(args.output)

if __name__ == '__main__':
    main()

import os
import pathlib
import numpy as np
import re
import matplotlib.pyplot as plt
from PIL import Image
'''本脚本实现特征图可视化'''
dir='/mnt/diske/lh/steDta/models'
timestamp='vtestres-s32d1div2k200'
load_path=os.path.join(dir,timestamp)
name='featureAve8.npy'
load_path=os.path.join(load_path,name)
loadData = np.load(load_path,allow_pickle=True)#(3,2,32,360,360)
ave=loadData[0]
lastFeature=loadData[1]
lastCover=loadData[2]

print(ave.shape)
print(lastFeature.shape)
print(lastCover.shape)
#平均图像
im = np.transpose(ave, [0,2, 3, 1])
plt.figure(1)
for k in range(2):
    # show top 12 feature maps
    for i in range(32):
        idx=k*32+i+1
        ax = plt.subplot(8, 8, idx)
        # [H, W, C]
        plt.title(str(idx))
        gray=im[k,:, :, i]
        plt.imshow(gray, cmap='gray')
plt.show()
#单张特征图
im = np.transpose(lastFeature, [0,2, 3, 1])
plt.figure(2)
for k in range(2):
    # show top 12 feature maps
    for i in range(32):
        idx=k*32+i+1
        ax = plt.subplot(8, 8, idx)
        # [H, W, C]
        plt.title(str(idx))
        gray=im[k,:, :, i]
        plt.imshow(gray, cmap='gray')
plt.show()
#单张原图
im = np.transpose(lastCover, [1, 2, 0])
plt.figure(3)
plt.title('cover')
plt.imshow(im)
plt.show()